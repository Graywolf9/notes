---
title: "Website"
---

# Website (with zola)

## Install zola (getzola.org)

## Create site

```
zola init nombre
cd nombre
```

## Get theme

Search theme you can look at: https://www.getzola.org/themes/

```
git submodule add https://github.com/codeandmedia/zola_easydocs_theme themes/zola_easydocs_theme
```

Set the theme name in global `config.toml` in first section vars

```
# The site theme to use.
theme = "zola_easydocs_theme"
```

## Clone the repo with the theme (submodule)

```
git clone --recursive git@gitlab.com:Graywolf9/notes.git
```
