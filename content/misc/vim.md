---
title: "Vim"
---

### Block replace

1. <kbd>Ctrl</kbd>+<kbd>b</kbd> , to enter into visual block
2. Make your selection
3. <kbd>c</kbd>, to change
4. <kbd>Esc</kbd>, to close an apply

### Create directory if new in the new file

```
:!mkdir -p %:h
```