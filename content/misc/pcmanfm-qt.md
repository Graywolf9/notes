---
title: "PCManFM-Qt"
---

### Agregar a menu contextual

Agregar archivo en `~/.local/share/file-manager/actions/nombre.desktop

```
[Desktop Entry]
Type=Action
Name=Nombre
Profiles=profile

[X-Action-Profile profile]
MimeTypes=inode/directory
Exec=/path/to/script
```
