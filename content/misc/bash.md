---
title: "Bash"
---

### Show progress bar

```bash
pv file.tar.gz | tar -xz
```

Nota: Da errores con `tar.xz`

For `tar.xz`

```bash
pv file.tar.xz | xzcat | tar -x
```

Just visual dots

```bash
tar -xzf file.tar.gz --checkpoint=.100
```

For `tar.xz`

```bash
unxz -v --stdout file.xz | tar -x
```