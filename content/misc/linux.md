---
title: "Linux"
---

## How to Check Disk Type SSD in Linux

ROTA shows if it is a HDD (1)

```
lsblk -d -o name,rota,size,type,mountpoints
```

## Pass password


```
echo "123" | sudo -S apt install vim
```

```
pkexec apt install vim
```

```
lxsudo apt install vim
```

```
gksudo apt install vim
```

## Get OSX icon theme

```
git clone https://github.com/ok9xnirab/macOs-icon-theme ~/.icons/La-Sierra
```

## Bash run from URL


```
curl -s http://example.com/script.sh | bash
```

```
wget -qO - http://example.com/script.sh | bash
```

## Disable kworker on Fedora

Missing how to identify the kworker

```
echo "disable" > /sys/firmware/acpi/interrupts/gpe06
```

## symbolic links on exfat (?)

make an img to format in ext4, mount into your desired folder and then you can

```bash
dd if=/dev/null of=example.img bs=1M seek=10240
mkfs.ext4 -F example.img
mkdir /mnt/example
mount -t ext4 -o loop example.img /mnt/example
sudo chown $USER: /mnt/example
```

Posible opción para cambiar tamaño (no tested)

```bash
resize2fs <image_name> <size><K/M/G>
resize2fs /dev/loopX <size><K/M/G>
```