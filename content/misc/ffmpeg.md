---
title: "ffmpeg"
---

### Cambiar la dimensión de un video y rellenar con fondo negro

```
ffmpeg -I $1 -ss 00:00:00 -t 01:11:44 -filter:v "scale=1280x720:force_original_aspect_ratio=decrease,pad=1280:720:-1:-1:color=black" -async 1 $1_output.mp4
```
