---
title: "Git"
---

## List all different files from current branch and main branch

```
git --no-pager diff --name-only FETCH_HEAD $(git merge-base FETCH_HEAD main)
```

## List all the files in a commit

```
git diff-tree --no-commit-id --name-only bd61ad98 -r
```

```
git show --pretty="" --name-only bd61ad98
```

## reset current branch and remove untracked directories and files

```
git reset --hard HEAD && git clean -fd
```

## show modified and new files

```bash
git ls-files -mo
```