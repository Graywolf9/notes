---
title: "PostgreSQL"
---

### Crear usuario

```sql
CREATE USER nombre WITH PASSWORD 'password';
```

### Create db

```bash
createdb nombre
```

ó

```sql
CREATE DATABASE nombre WITH OWNER = nombre
```

#### List databases

```
\l
```

#### List tables

```
\dt
```

#### Delete db

```
dropdb name
```

### Nota: pg_ctl es un comando interno de pg_ctlcluster o pg_ctrlcluster

```bash
find /usr/lib/postgresql/ -name pg_ctl
```

### `pg_hba.conf` Configuración de usuarios

```
local	all	postgres		peer
```

#### con contraseña

```
local	all	postgres		md5
```

### Extensiones

#### See available, run in psql

```sql
SELECT * FROM pg_available_extensions ORDER BY name;
```
#### In the db

```sql
CREATE EXTENSION unaccent;
```
#### Corroborar, run in db

```sql
SELECT * FROM pg_available_extensions WHERE installed_version IS NOT NULL ORDER BY name;
```

#### PRIVILEGES

```sql
GRANT ALL PRIVILEGES ON DATABASE "my_db" to my_user;
```