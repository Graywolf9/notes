---
title: "emacs"
---

### De formato de código a script (tangle)

Atajo de teclas `C-c C-v t`

```
#+BEGIN_SRC shell :tangle ~/scripts/hello.sh
echo "Hola mundo"
#+END_SRC
```
### Cheatsheet

`C-x` == <kbd>Ctrl<kbd> + <kbd>x</kbd>

`C-x C-s`: Save

`C-x C-c`: Close

`C-x C-f`: Find file

### Edit dirs (Bulk renaming)

`C-x C-q`: Make dired buffer writeable
`Esc`: Using evil mode to enter into normal mode
`:`: To enter your replace command
    :%s/target/text/g
`C-c C-c`: Write changes